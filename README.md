# NearbyShops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.3.

### [Demo](https://app-nearby-shops.herokuapp.com)

This codebase was created in reponse to a job application coding challenge built with Angular that interacts with an actual backend server including CRUD operations, authentication, routing. We've gone to great lengths to adhere to the [Angular Styleguide](https://angular.io/styleguide) & best practices.

### Making requests to the backend API

For convenience, I have a live API server running at https://nearby-shops-backend.herokuapp.com/api for the application to make requests against.

The source code for the backend server (built with Node, Express and MongoDB) can be found in the [Backend API repo](https://bitbucket.org/himstar88/rest-api).

If you want to change the API URL to a local server, simply edit `src/environments/environment.ts` and change `api_url` to the local server's URL (i.e. `localhost:3000/api`).

# Getting started

Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally. I use [NPM](https://www.npmjs.com) to manage the dependencies, so I strongly recommend you to use it. It comes with Node.js so if you don't have Node installed in your machine you can install it from [Here](https://nodejs.org), then run `npm install` to resolve all dependencies (might take a minute).

Get the backend API from [Here](https://bitbucket.org/himstar88/rest-api).
Run de Backend server `npm run dev`
Run the app `npm run start-dev` for a dev server. Navigate to `http://localhost:4200/`.

## Functionality overview

The application uses Google Place Api to list shops nearby via geolocation. It uses a custom API for all requests, including authentication. You can view a live demo over at https://app-nearby-shops.herokuapp.com

**General functionality:**

- Authenticate users via JWT
- CRU\* users (sign up & settings page - no deleting required)
- CR\*D Shops (no updating required)
- Like a shop so it can be added to my preferred shops
- Dislike a shop so it won't be so it won’t be displayed within “Nearby Shops” list during the next 2 hours
- GET and display paginated lists of liked shops

**The general page breakdown looks like this:**

- There is no home page. the (URL: /#/ ) redirect the user either on the login (URL: /#/login ) page or the "Nearby shops" (URL: /#/nearby-shops ) page depending on loggedin status

- Sign in page (URL: /#/login)
  - Uses JWT
  - Use NGRX state management to store user's token via ngrx localstorageSync
- Sign up pages (URL: /#/register )
  - Check for email uniqueness as user typing his email
  - Require a password of 5 charcters or more
- Nearby shops page to view like and dislike shops (URL: /#/nearby-shops)
  - Geolocation. If Geolocation fails ask to use a default position. If browser do not support geolocation use the default position
  - Fetch user preferred shops from the backend API in order to filter out them from the google api call results if any exist
  - Fetch from Google Api the nearest shops within a radius of 16000
  - Filter out the disliked shops if not expired (< 2h)
  - Pagination for list of shops
  - Like a shop so it can be added to user's preferred shops list (save into the database)
  - Dislike a shop (shop won't be visible during the next 2h)
- Preferred shops page (URL: /#/preferred-shops )
  - List shops that user liked (fetched from the Backend API)
  - Pagination for list of preferred shops
  - User can remove a shop from this list

<br />
