import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/core/store';
import { Logout } from 'src/app/core/store/actions/auth.actions';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
  constructor(private store: Store<State>) {}

  isOpened = false;

  ngOnInit() {}

  navbarLinkClick() {
    const isSidebarOpened = document
      .querySelector('body')
      .classList.contains('navbar-open');
    if (isSidebarOpened) {
      this.isOpened = false;
      document.querySelector('body').classList.remove('navbar-open');
    }
  }

  backdropClick() {
    this.isOpened = false;
    document.querySelector('body').classList.remove('navbar-open');
  }

  onLogout() {
    this.store.dispatch(new Logout());
  }
}
