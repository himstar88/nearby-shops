import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { NearbyShopsComponent } from './pages/nearby-shops/nearby-shops.component';
import { PreferredShopsComponent } from './pages/preferred-shops/preferred-shops.component';
import { AuthGuard } from './core/guards/auth.guard';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  { path: 'register', component: RegisterComponent },
  {
    path: 'nearby-shops',
    component: NearbyShopsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'preferred-shops',
    component: PreferredShopsComponent,
    canActivate: [AuthGuard]
  },
  { path: '', redirectTo: 'nearby-shops', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
