import { Action } from '@ngrx/store';
import { Shop, Timer } from '../reducers/shop.reducer';

export enum ShopActionTypes {
  LOAD_SHOPS = '[Shops] Load Shops',
  LOAD_SUCCESS = '[Shops] Load success',
  LOAD_FAILURE = '[Shops] Load failure',
  LOAD_PREFERRED_SHOPS = '[Shops] Load preferred shops',
  LOAD_PREFERRED_SHOPS_SUCCESS = '[Shops] Load preferred shops success',
  LOAD_PREFERRED_SHOPS_FAILURE = '[Shops] Load preferred shops failure',
  LIKE_SHOP = '[Shops] Like shop',
  DISLIKE_SHOP = '[Shops] Dislike shop',
  REMOVE_SHOP = '[Shops] Remove Shop',
  CLEAR_DATA = '[Shops] Clear data'
}

export class LoadShops implements Action {
  readonly type = ShopActionTypes.LOAD_SHOPS;
  constructor(
    public payload: {
      lat: number;
      lng: number;
      keyword?: string;
      type?: string;
    }
  ) {}
}
export class LoadShopsSuccess implements Action {
  readonly type = ShopActionTypes.LOAD_SUCCESS;
  constructor(public payload: any) {}
}
export class LoadShopsFailure implements Action {
  readonly type = ShopActionTypes.LOAD_FAILURE;
}
export class LoadPreferredShops implements Action {
  readonly type = ShopActionTypes.LOAD_PREFERRED_SHOPS;
}
export class LoadPreferredShopsSuccess implements Action {
  readonly type = ShopActionTypes.LOAD_PREFERRED_SHOPS_SUCCESS;
  constructor(public payload: any[]) {}
}
export class LoadPreferredShopsFailure implements Action {
  readonly type = ShopActionTypes.LOAD_PREFERRED_SHOPS_FAILURE;
}
export class LikeShop implements Action {
  readonly type = ShopActionTypes.LIKE_SHOP;
  constructor(public payload: Shop) {}
}
export class DislikeShop implements Action {
  readonly type = ShopActionTypes.DISLIKE_SHOP;
  constructor(public payload: Timer) {}
}
export class RemoveShop implements Action {
  readonly type = ShopActionTypes.REMOVE_SHOP;
  constructor(public id: string) {}
}

export class ClearShopsData implements Action {
  readonly type = ShopActionTypes.CLEAR_DATA;
}

export type ShopActions =
  | LoadShops
  | LoadShopsSuccess
  | LoadShopsFailure
  | LoadPreferredShops
  | LoadPreferredShopsSuccess
  | LoadPreferredShopsFailure
  | LikeShop
  | DislikeShop
  | RemoveShop
  | ClearShopsData;
