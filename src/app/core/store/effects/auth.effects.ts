import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import * as AuthActions from '../actions/auth.actions';
import { switchMap, map, tap, catchError, take } from 'rxjs/operators';
import { AuthenticationService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private _authService: AuthenticationService,
    private router: Router,
    private _alert: ToastrService
  ) {}

  // Login effects
  @Effect()
  Login$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.LOGIN),
    map((action: AuthActions.Login) => action.payload),
    switchMap(payload => {
      return this._authService.login(payload.email, payload.password).pipe(
        map(user => {
          return new AuthActions.LoginSuccess({
            token: user.token,
            email: payload.email
          });
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new AuthActions.LoginFailure(error));
        })
      );
    })
  );

  // Login Success effects //
  @Effect({ dispatch: false })
  LoginSuccess$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.LOGIN_SUCCESS),
    tap(() => {
      this.router.navigateByUrl('/nearby-shops');
    })
  );

  // Login Failure effects //
  @Effect({ dispatch: false })
  LoginFailure$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.LOGIN_FAILURE),
    map((action: AuthActions.LoginFailure) => action.payload),
    tap((err: HttpErrorResponse) => {
      if (err.status === 401) {
        this._alert.error('Invalid credentials');
      }
    })
  );

  // Register effects //
  @Effect()
  Register$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.REGISTER),
    map((action: AuthActions.Register) => action.payload),
    switchMap(payload => {
      return this._authService.register(payload.email, payload.password).pipe(
        map(user => {
          return new AuthActions.RegisterSuccess();
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new AuthActions.RegisterFailure(error));
        })
      );
    })
  );

  // Register Success effects //
  @Effect({ dispatch: false })
  RegisterSuccess$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.REGISTER_SUCCESS),
    tap(() => {
      this._alert.success('Successfuly registred');
      this.router.navigateByUrl('/login');
    })
  );
  // Register Failure effects //
  @Effect({ dispatch: false })
  RegisterFailure$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.REGISTER_FAILURE),
    take(1),
    map((action: AuthActions.RegisterFailure) => action.payload),
    tap((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this._alert.error('Unable to validate your info, Please retry');
      }
      this._alert.error('Unknown error');
    })
  );

  // Logout effect
  @Effect({ dispatch: false })
  public Logout$: Observable<any> = this.actions$.pipe(
    ofType(AuthActions.AuthActionTypes.LOGOUT),
    tap(() => {
      this.router.navigateByUrl('/login');
    })
  );
}
