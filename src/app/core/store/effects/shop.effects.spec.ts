import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ShopEffects } from './shop.effects';

describe('ShopEffects', () => {
  let actions$: Observable<any>;
  let effects: ShopEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ShopEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(ShopEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
