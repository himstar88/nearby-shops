import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromAction from '../actions/shop.actions';
import { Observable, of, empty, throwError } from 'rxjs';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  tap,
  mapTo
} from 'rxjs/operators';
import { ShopService } from '../../services/shops.service';
import { SearchService } from '../../services/serach.service';
import { Store, select } from '@ngrx/store';
import { State } from '..';
import { selectShopById, selectAllShops } from '../reducers/shop.reducer';
import { ToastrService } from 'ngx-toastr';
import { AuthActionTypes } from '../actions/auth.actions';

@Injectable()
export class ShopEffects {
  constructor(
    private actions$: Actions,
    private _shopService: ShopService,
    private _search: SearchService,
    private store: Store<State>,
    private _alert: ToastrService
  ) {}

  @Effect()
  LoadPreferredShops$: Observable<any> = this.actions$.pipe(
    ofType(fromAction.ShopActionTypes.LOAD_PREFERRED_SHOPS),
    switchMap(() => {
      return this._shopService.getLikedShops().pipe(
        map(res => {
          return new fromAction.LoadPreferredShopsSuccess(res.shops);
        }),
        catchError(err => {
          console.log(err);
          return of(new fromAction.LoadPreferredShopsFailure());
        })
      );
    })
  );

  @Effect()
  LoadShops$: Observable<any> = this.actions$.pipe(
    ofType(fromAction.ShopActionTypes.LOAD_SHOPS),
    map((action: fromAction.LoadShops) => action.payload),
    switchMap(data => {
      return this._search
        .sendSearchRequest(data.lat, data.lng, data.keyword, data.type)
        .pipe(
          map(res => {
            const shops = res.results.map(item => {
              item.photo_reference = this.extractPhotosReferences(item.photos);
              return {
                id: item.id,
                name: item.name,
                place_id: item.place_id,
                photo_reference: item.photo_reference,
                rating: item.rating,
                types: item.types,
                vicinity: item.vicinity
              };
            });

            return new fromAction.LoadShopsSuccess(shops);
          }),
          catchError(err => {
            return of(new fromAction.LoadShopsFailure());
          })
        );
    })
  );

  @Effect({ dispatch: false })
  LikeShop$: Observable<any> = this.actions$.pipe(
    ofType(fromAction.ShopActionTypes.LIKE_SHOP),
    map((action: fromAction.LikeShop) => action.payload),
    switchMap(shop => {
      const { name, place_id, rating, photo_reference, types, vicinity } = shop;
      return this._shopService
        .addShop({ name, place_id, rating, photo_reference, types, vicinity })
        .pipe(
          map(res => {
            this._alert.success('Shop added to Preferred list');
            return of(null);
          }),
          catchError(err => {
            this._alert.error('Fail to add shop to the preferred list');
            return throwError(err);
          })
        );
    })
  );

  @Effect({ dispatch: false })
  DislikeShop$: Observable<any> = this.actions$.pipe(
    ofType(fromAction.ShopActionTypes.DISLIKE_SHOP),
    tap(() => {
      this._alert.success(`This shop won't be visible during the next 2h`);
    })
  );

  @Effect({ dispatch: false })
  RemoveShop$: Observable<any> = this.actions$.pipe(
    ofType(fromAction.ShopActionTypes.REMOVE_SHOP),
    map((action: fromAction.RemoveShop) => action.id),
    switchMap(id => {
      return this._shopService.removeShop(id).pipe(
        map(res => {
          this._alert.success('Shop successefully removed');
          return of(empty);
        }),
        catchError(err => {
          this._alert.warning('Fail to remove shop');
          return throwError(err);
        })
      );
    })
  );

  @Effect()
  public Logout$: Observable<any> = this.actions$.pipe(
    ofType(AuthActionTypes.LOGOUT),
    mapTo(new fromAction.ClearShopsData())
  );

  extractPhotosReferences(photo: any[]) {
    return photo ? (photo[0].photo_reference as string) : 'Photo unavailable';
  }
}
