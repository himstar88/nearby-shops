import { RouterReducerState, routerReducer } from '@ngrx/router-store';
import {
  ActionReducerMap,
  MetaReducer,
  createFeatureSelector,
  ActionReducer,
  Action
} from '@ngrx/store';
import * as auth from './reducers/auth.reducer';
import * as shops from './reducers/shop.reducer';

import { localStorageSync } from 'ngrx-store-localstorage';

const STORE_KEYS_TO_PERSIST = ['auth', 'shops'];

export interface State {
  auth: auth.AuthState;
  shops: shops.ShopState;
  router: RouterReducerState;
}

export const reducers: ActionReducerMap<State> = {
  auth: auth.reducer,
  shops: shops.reducer,
  router: routerReducer
};

export function localStorageSyncReducer(
  reducer: ActionReducer<State>
): ActionReducer<State> {
  return localStorageSync({
    keys: STORE_KEYS_TO_PERSIST,
    rehydrate: true
  })(reducer);
}

export const metaReducers: Array<MetaReducer<State, Action>> = [
  localStorageSyncReducer
];

// Selectors
export const selectAuthState = createFeatureSelector<State>('auth');
export const selectRouterState = createFeatureSelector<State>('router');

export const selectIsAuthenticated = (state: State) =>
  state.auth.isAuthenticated;

export const selectToken = (state: State) => state.auth.user;
