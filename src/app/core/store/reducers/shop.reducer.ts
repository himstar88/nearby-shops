import { Action, createSelector } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import * as fromActions from '../actions/shop.actions';
import * as fromRoot from '../index';

export interface Shop {
  id: string;
  name: string;
  place_id: string;
  photo_reference: string;
  rating?: number;
  types?: any[];
  vicinity?: string;
}

export interface Timer extends Shop {
  timer: number;
}

// Start by defining a state of each entity
interface AllShopState extends EntityState<Shop> {
  isLoading: boolean;
}
interface PreferredShopState extends EntityState<Shop> {
  isLoading: boolean;
}

interface DislikedShopState extends EntityState<Timer> {}

// Then create a state that hold the entities
export interface ShopState {
  allShops: AllShopState;
  preferredShops: PreferredShopState;
  dislikedShops: DislikedShopState;
}

// Create the adapter for each entity state to manage the data and create the initial state
const allShopsAdapter = createEntityAdapter<Shop>();
const preferredShopsAdapter = createEntityAdapter<Shop>();
const dislikedShopsAdapter = createEntityAdapter<Timer>();

const allShopsInitialState: AllShopState = allShopsAdapter.getInitialState({
  isLoading: false
});

const preferredShopsInitialState: PreferredShopState = preferredShopsAdapter.getInitialState(
  {
    isLoading: false
  }
);

const dislikedShopsInitialState: DislikedShopState = dislikedShopsAdapter.getInitialState();

// Define the initial global state
export const initialState = {
  allShops: allShopsInitialState,
  preferredShops: preferredShopsInitialState,
  dislikedShops: dislikedShopsInitialState
};

export function reducer(
  state = initialState,
  action: fromActions.ShopActions
): ShopState {
  switch (action.type) {
    case fromActions.ShopActionTypes.LOAD_SHOPS:
      return {
        ...state,
        allShops: { ...state.allShops, isLoading: true }
      };
    case fromActions.ShopActionTypes.LOAD_PREFERRED_SHOPS:
      return {
        ...state,
        preferredShops: { ...state.preferredShops, isLoading: true }
      };

    case fromActions.ShopActionTypes.LOAD_SUCCESS:
      let IntShopState = allShopsAdapter.addMany(
        action.payload,
        state.allShops
      );
      IntShopState = { ...IntShopState, isLoading: false };
      return {
        ...state,
        allShops: IntShopState
      };

    case fromActions.ShopActionTypes.LOAD_PREFERRED_SHOPS_SUCCESS:
      let IntermediatePreferredShopState = preferredShopsAdapter.addMany(
        action.payload,
        state.preferredShops
      );
      IntermediatePreferredShopState = {
        ...IntermediatePreferredShopState,
        isLoading: false
      };
      return {
        ...state,
        preferredShops: IntermediatePreferredShopState
      };

    case fromActions.ShopActionTypes.LIKE_SHOP:
      return {
        ...state,
        allShops: allShopsAdapter.removeOne(action.payload.id, state.allShops)
      };

    case fromActions.ShopActionTypes.DISLIKE_SHOP:
      return {
        ...state,
        allShops: allShopsAdapter.removeOne(action.payload.id, state.allShops),
        dislikedShops: dislikedShopsAdapter.addOne(
          action.payload,
          state.dislikedShops
        )
      };

    case fromActions.ShopActionTypes.REMOVE_SHOP:
      return {
        ...state,
        preferredShops: preferredShopsAdapter.removeOne(
          action.id,
          state.preferredShops
        )
      };
    case fromActions.ShopActionTypes.CLEAR_DATA:
      return initialState;
    default:
      return state;
  }
}

export const selectAllShopsState = (state: fromRoot.State) =>
  state.shops.allShops;
export const selectPreferredShopsState = (state: fromRoot.State) =>
  state.shops.preferredShops;

export const selectDislikedShopsState = (state: fromRoot.State) =>
  state.shops.dislikedShops;

// Expose the selectors
const {
  selectIds: AllshopsIds,
  selectEntities: AllshopsEntities,
  selectAll: AllShops,
  selectTotal: AllshopsCount
} = allShopsAdapter.getSelectors();

const {
  selectIds: PreferredShopsIds,
  selectEntities: PreferredShopsEntities,
  selectAll: AllPreferredShops,
  selectTotal: PreferredShopsCount
} = preferredShopsAdapter.getSelectors();

const {
  selectIds: DislikedShopsIds,
  selectEntities: DislikedShopsEntities,
  selectAll: AllDislikedShops,
  selectTotal: DislikedShopsCount
} = dislikedShopsAdapter.getSelectors();

export const selectPreferredShops = createSelector(
  selectPreferredShopsState,
  AllPreferredShops
);

export const selectAllShops = createSelector(
  selectAllShopsState,
  AllShops
);

export const selectDislikedShops = createSelector(
  selectDislikedShopsState,
  AllDislikedShops
);

export const selectAllShopsLoadingState = createSelector(
  selectAllShopsState,
  state => state.isLoading
);

export const selectPreferredShopsLoadingState = createSelector(
  selectPreferredShopsState,
  state => state.isLoading
);

export const selectShopById = (id: string) =>
  createSelector(
    selectAllShops,
    shops => shops.find(item => item.id === id)
  );
