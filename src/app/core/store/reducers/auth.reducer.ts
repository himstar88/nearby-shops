import { Action, createSelector } from '@ngrx/store';
import { User } from 'src/app/shared/models/user.model';
import { AuthActionTypes, AuthActions } from '../actions/auth.actions';
import * as fromRoot from '../index';

export interface AuthState {
  // is a user authenticated ?
  isAuthenticated: boolean;

  // if authenticated there should be a user object
  user: User | null;

  // error message
  errorMessage: string | null;

  isLoading: boolean;
}

// Initiale state
export const initialState: AuthState = {
  isAuthenticated: false,
  user: null,
  errorMessage: null,
  isLoading: false
};

export function reducer(state = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN:
      return {
        ...state,
        isLoading: true
      };
    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token,
          email: action.payload.email
        },
        errorMessage: null,
        isLoading: false
      };
    case AuthActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        isLoading: false
      };
    case AuthActionTypes.REGISTER:
      return {
        ...state,
        isLoading: true
      };
    case AuthActionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        errorMessage: null,
        isLoading: false
      };
    case AuthActionTypes.REGISTER_FAILURE: {
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        isLoading: false
      };
    }
    case AuthActionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
}

export const selectErrorMessage = (state: fromRoot.State) =>
  state.auth.errorMessage;

export const selectAuthLoadingState = (state: fromRoot.State) =>
  state.auth.isLoading;
