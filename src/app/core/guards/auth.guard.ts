import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/auth.service';
import { Store, select } from '@ngrx/store';
import { State, selectIsAuthenticated } from '../store/index';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  isAuth: boolean;
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private store: Store<State>
  ) {
    this.store.pipe(select(selectIsAuthenticated)).subscribe(isAuth => {
      this.isAuth = isAuth;
    });
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.isAuth) {
      this.router.navigate(['/login']);
    }
    return true;
  }
}
