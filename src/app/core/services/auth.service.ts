import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

const cors = 'https://cors-anywhere.herokuapp.com/';

const base_url = environment.production
  ? cors + environment.api_url + '/users/'
  : 'api/users/';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string) {
    return this.http.post<any>(base_url + 'login', { email, password });
  }

  register(email: string, password: string) {
    return this.http.post(base_url + 'register', {
      email,
      password
    });
  }

  // Check if email is taken
  checkEmail(email: string) {
    return this.http.post<{ message: string } | null>(base_url + 'email', {
      email
    });
  }
}
