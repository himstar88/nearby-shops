import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Shop } from '../store/reducers/shop.reducer';
import { environment } from 'src/environments/environment';

const cors = 'https://cors-anywhere.herokuapp.com/';

const base_url = environment.production ? cors + environment.api_url : 'api';

@Injectable({ providedIn: 'root' })
export class ShopService {
  constructor(private http: HttpClient) {}

  addShop(shop: Partial<Shop>) {
    return this.http.post(base_url + '/shops/add', shop);
  }

  getLikedShops() {
    return this.http.get<{ shops: any[] }>(base_url + '/users/shops');
  }

  removeShop(id: string) {
    return this.http.delete(base_url + `/shops/remove/${id}`);
  }
}
