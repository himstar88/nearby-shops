import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const api_key = 'AIzaSyB2dGnQCr3Aej6iGVESCbZLxMFqCS1pbm4';
const cors = 'https://cors-anywhere.herokuapp.com/';

const search_base_url =
  cors +
  'https://maps.googleapis.com/maps/api/place/nearbysearch/json?&key=' +
  api_key +
  '&radius=16000' +
  '&sensor=false';

// tslint:disable-next-line:max-line-length
export const pull_photo_url = `${cors}https://maps.googleapis.com/maps/api/place/photo?maxheight=1600&maxwidth=1600&key=${api_key}&photoreference=`;

export interface Place {
  geometry: object;
  photos: any[];
  photo_reference: string;
  icon: string;
  id: string;
  name: string;
  place_id: string;
  rating: number;
  types: any[];
  vicinity: string;
}

@Injectable({ providedIn: 'root' })
export class SearchService {
  constructor(private httpClient: HttpClient) {}

  sendSearchRequest(
    lat: number,
    lng: number,
    keyword = 'shop',
    type = 'restaurant'
  ) {
    return this.httpClient.get<{ results: Place[] }>(
      search_base_url +
        `&keyword=${keyword}&location=${lat},${lng}&types=${type}`
    );
  }
}
