import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/core/store';
import { Login } from 'src/app/core/store/actions/auth.actions';
import { selectAuthLoadingState } from 'src/app/core/store/reducers/auth.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: []
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isLoading$: Observable<boolean>;

  constructor(private router: Router, private store: Store<State>) {}

  ngOnInit() {
    this.isLoading$ = this.store.pipe(select(selectAuthLoadingState));

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }

  onSubmit(form: FormGroup) {
    // Make sure our form is valid before sending any request.
    if (form.valid) {
      const email = form.value.email;
      const password = form.value.password;

      // Dispatch the login action
      this.store.dispatch(new Login({ email, password }));
    } else if (!this.loginForm.touched || this.loginForm.pristine) {
      // In case the user tries to submit the form without touching the form
      // We mark form controls as touched to display their according error message
      Object.keys(this.loginForm.controls).forEach(field => {
        const control = this.loginForm.get(field);
        control.markAsTouched({ onlySelf: true });
      });

      // Show an alert message
      // this._alert.error(
      //   'Veuillez remplir les informations de connexion',
      //   false
      // );
    }
  }
}
