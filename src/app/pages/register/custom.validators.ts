import { AbstractControl } from '@angular/forms';

import { map, catchError, debounceTime, take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { of } from 'rxjs';

export function passwordMatchValidator(control: AbstractControl) {
  const password = control.get('password').value;
  const confirmPassword = control.get('confirmPassword').value;

  return password === confirmPassword ? null : { passwordsNotEqual: true };
}

export class UniqueEmailValidator {
  static createValidator(service: AuthenticationService) {
    return (control: AbstractControl) => {
      return service.checkEmail(control.value).pipe(
        debounceTime(5000),
        take(1),
        map(res => {
          return res ? res : null;
        }),
        catchError(() => of(null))
      );
    };
  }
}
