import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {
  passwordMatchValidator,
  UniqueEmailValidator
} from './custom.validators';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/core/store';
import { Register } from 'src/app/core/store/actions/auth.actions';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { Observable } from 'rxjs';
import { selectAuthLoadingState } from 'src/app/core/store/reducers/auth.reducer';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isLoading$: Observable<boolean>;

  constructor(
    private authService: AuthenticationService,
    private store: Store<State>
  ) {}

  ngOnInit() {
    this.isLoading$ = this.store.pipe(select(selectAuthLoadingState));

    this.registerForm = new FormGroup(
      {
        email: new FormControl(
          '',
          [Validators.required, Validators.email],
          [UniqueEmailValidator.createValidator(this.authService)]
        ),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(5)
        ]),
        confirmPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(5)
        ])
      },
      { validators: passwordMatchValidator }
    );
  }

  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get confirmPassword() {
    return this.registerForm.get('password');
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      const email = form.value.email;
      const password = form.value.password;

      this.store.dispatch(new Register({ email, password }));
    }
  }
}
