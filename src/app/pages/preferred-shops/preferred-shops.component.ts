import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/core/store';
import * as shopActions from 'src/app/core/store/actions/shop.actions';
import * as shopReducer from 'src/app/core/store/reducers/shop.reducer';
import { trigger, transition, style, animate } from '@angular/animations';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-preferred-shops',
  templateUrl: './preferred-shops.component.html',
  styleUrls: ['./preferred-shops.component.css'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }), // initial
        animate('1s ease-out', style({ transform: 'scale(1)', opacity: 1 })) // final
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1 }),
        animate(
          '1s ease-out',
          style({
            transform: 'scale(0.7)',
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class PreferredShopsComponent implements OnInit {
  dataSource: any;
  array: any[];

  currentPage = 0;
  totalSize = 0;
  pageSize = 5;

  isLoading$: Observable<boolean>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.isLoading$ = this.store.pipe(
      select(shopReducer.selectPreferredShopsLoadingState)
    );

    this.store.dispatch(new shopActions.LoadPreferredShops());

    this.getShops();
  }

  public handlePage(event: any) {
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }

  private getShops() {
    this.store
      .pipe(select(shopReducer.selectPreferredShops))
      .subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.array = data;
        this.totalSize = data.length;
        this.iterator();
      });
  }

  onRemove(id: string) {
    this.store.dispatch(new shopActions.RemoveShop(id));
  }
}
