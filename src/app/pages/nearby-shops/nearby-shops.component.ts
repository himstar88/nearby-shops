import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import * as shopReducer from 'src/app/core/store/reducers/shop.reducer';
import { trigger, animate, style, transition } from '@angular/animations';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/core/store';
import * as shopActions from 'src/app/core/store/actions/shop.actions';
import { withLatestFrom, map } from 'rxjs/operators';

@Component({
  selector: 'app-nearby-shops',
  templateUrl: './nearby-shops.component.html',
  styleUrls: ['./nearby-shops.component.css'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.9)', opacity: 0 }), // initial
        animate('1s ease-in', style({ transform: 'scale(1)', opacity: 1 })) // final
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1 }),
        animate(
          '1s ease-out',
          style({
            transform: 'scale(0.9)',
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class NearbyShopsComponent implements OnInit {
  dataSource: any;
  array: any[];

  currentPage = 0;
  totalSize = 0;
  pageSize = 5;

  isLoading$: Observable<boolean>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.geolocate();
    this.isLoading$ = this.store.pipe(
      select(shopReducer.selectAllShopsLoadingState)
    );
    this.getShops();
  }

  public handlePage(event: any) {
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }

  private getShops() {
    // Load preferred shops in order to filter out them from the fetched shops
    this.store.dispatch(new shopActions.LoadPreferredShops());

    this.store
      .pipe(
        select(shopReducer.selectAllShops),
        withLatestFrom(this.store.select(shopReducer.selectDislikedShops)),
        withLatestFrom(this.store.select(shopReducer.selectPreferredShops)),
        map(data => {
          // Get disliked shops which does not expired
          const dislikedShops = data[0][1].filter(item =>
            this.computeExp(item)
          );

          // Remove disliked shops which are not expired
          let shops = data[0][0].filter(item =>
            this.removeDislikedShop(item, dislikedShops)
          );

          // Remove liked shops if any exist
          const likedShops = data[1];
          shops = shops.filter(item => this.removeLikedShop(item, likedShops));

          return shops;
        })
      )
      .subscribe(shops => {
        this.dataSource = new MatTableDataSource(shops);
        this.dataSource.paginator = this.paginator;
        this.array = shops;
        this.totalSize = shops.length;
        this.iterator();
      });
  }

  private computeExp(shop: shopReducer.Timer) {
    const now = new Date().getTime();
    const exp = shop.timer - now;
    return exp > 0 ? true : false;
  }

  private removeDislikedShop(shop: shopReducer.Shop, arr: shopReducer.Timer[]) {
    return arr.find(item => item.id === shop.id) ? false : true;
  }

  private removeLikedShop(shop: shopReducer.Shop, arr: shopReducer.Shop[]) {
    return arr.find(item => item.place_id === shop.place_id) ? false : true;
  }

  onLike(shop: shopReducer.Shop) {
    this.store.dispatch(new shopActions.LikeShop(shop));
  }

  onDislike(shop: shopReducer.Shop) {
    // Set disliked shop expiration time
    const timer = { timer: new Date().getTime() + 7200000 };
    const dislikedshop = { ...shop, ...timer };

    this.store.dispatch(new shopActions.DislikeShop(dislikedshop));
  }

  private geolocate() {
    // Make sure geolocation is supported by the client browser
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          console.log(lat, lng);
          this.store.dispatch(new shopActions.LoadShops({ lat, lng }));
        },
        () => {
          // Error happened when trying to get the current position.
          console.log('Geolocation has failed');

          // Try to fetch shops with the default position
          if (
            confirm(
              'Geolocation has failed Miserably !! Do you want to use the default position ?'
            )
          ) {
            this.store.dispatch(
              new shopActions.LoadShops({ lat: 33.5452377, lng: -7.6756141 })
            );
          }
        }
      );
    } else {
      // Display error message when geolocation is not supported by the browser
      // and fetch shops with a default position
      alert('This browser does not support geolocation');
      this.store.dispatch(new shopActions.LoadShops({ lat: 33, lng: -7 }));
    }
  }
}
